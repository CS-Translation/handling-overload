Counter-protected process

计数器保护进程

The interesting bit with a bounded queue and the usage of mailboxes for processes 
in Erlang is that the queue from the previous section stops being required. 
We can go back to:

在erlang中为进程提供消息队列的用法以及带边界的队列所关注的bit，就是前面章节所提到
的停止入队所需要的，让我们回顾一下。

As long as a given server process accesses the counter (a mutex!) before sending
its message. In fact, that's probably how one would naively implement the queue 
from the previous section. The reason it's interesting to think of the counter 
as protecting a mailbox instead of a queue is that it lets us think about it as 
agumenting a process instead of adding an architectural component.

一个给定的进程只要在发消息前就要访问这个计数。实际上这或许是依据前面的章节最直接
的实现这个队列的方法。这种方法关注的是考虑用计数器来保护邮箱而不是队列的原因是：
这种方法让我们考虑无需增加架构组件，而仅仅是进程参数化。

Would there be value in putting one of these at every step of the way?
Technically, there would be little value in doing this, simply because if the 
first worker is doing synchronous work with the latter one, then the synchronous
aspect of execution means that a single counter (and therefore a single point of 
contention) should be sufficient to protect the entire pipeline; otherwise, the 
reate at which A can dequeue will be related.

在每一步都按照这种方式增加一个上述的计数器是否有价值呢？严格来讲，这样做意义不大，
仅仅是因为第一个worker和后面的一个会一起同步的工作，这种执行的同步意味着一个单一
的计数器足够保护整条流水线；否则，A的出队就会被关联起来。

This will not hold if operations between any of the workers become asnchronous 
again, and the same criticism holds for queues:

如果这些worker中的的任何一个再次变成异步，这种方法就会失效，对于队列的也存在同样
的问题。

If the communication from A to B is asynchronous, then the counter protecting A 
is mostly useless to help with total system stability: it prevents A from having 
its mailbox overflow, but B's own counter would have to do the same on its own 
if it is slower, and since B depends on C, then B implicitly protects C. Now, 
the counters in A and B are all possibly very useful.

如果A和B之间的通信是异步的，仅仅有保护A的计数器可能无法保证整个系统的稳定：这种
方式虽然防止了A出现邮箱过载，但是，如果B运行的更慢一些，B不得不像A一样拥有自己的
计数器，同时B依赖C，此时B间接的保护了C。此时A和B之间的计数器都非常有用。

Things get hairier to track when the messaging process is not a pipeline, but a 
graph of multiple processes that are mostly disorganised or into independent 
structures. Not only is ensuring good system balance tricky, but we now introduce 
all kinds of possibilities and asymmetric delays where parts of the system are 
clogged but not other ones, and if data crosses streams between subcomponents,
part of the pipeline can be super healthy while other bits of it a crawling with
pain.

当消息处理不是流水线的时候，跟踪消息就会变成一件极为困难的事情，多进程的图表就会
非常有可能是杂乱无章的，或者是进入一种孤立的组织结构中。我们不仅要保护好系统的
微妙平衡，我们要介绍各种可能的情况以及造成系统非对称延时的那些部分，如果数据在
这些组件中流过，流水线的某些部分会运行的十分健康，而其他部分会运行的及其艰难。

What transpires from this tought exercise is that the things we need to protect are:
1.all points of asynchrony that prevent direct feedback via messages
2.patterns where ever-increasing senders can effectively cancel the result of 
  direct feedback via messages
3.having a ton of queuing points makes it very hard to reason about the system

从这个思考实践中揭示出我们需要保护的事情如下：
1、所有的异步点尽量避免通过消息反馈
2、在发送者曾经激增的地方，这些模式可以有效的通过消息，取消反馈的结果
3、拥有大量入队点使得很难对系统进行推理。

This is interesting food for thought!
How to do this in Erlang
•just use ets:update_counter/3-4 to maintain lock values
•canal_lock for resource-adjustable dynamic locks for dynamic resources
•some of the pooling libraries mentioned above contain such mechanisms

这是一个令人感兴趣的思考大餐！
在erlang中如果做呢？



The Network Plays Ball

网络开始运行

In fact, this problem is specifically what happens in large networked applications 
when people go "the thing is slow". We have this constellation of networked 
services routed over multiple physical devices.

实际上，在大型网络应用中，当人们发现事情开始变慢时，究竟发生了什么，这个问题就会
凸显出来。 我们拥有一系列的网络服务运行在复杂的物理设备中。

The difference for a networked service is that there is no such thing as a big 
"node-global shared mutex or atomic counter everyone can hook into" to synchronize
things; everything is made much harder because it is done as an observer. In short, 
the network brings us back to early design decisions because we're stuck with
message passing and nothing fancier.

一个网络服务的难点在于没有一个全局的共享信号量或者原子计数器来同步所有事情。每一
件事情变得更加困难的原因在于他们在以观察者的身份被运行。简单来说，由于我们被过往
的消息困住并且无任何设想，使得网路引领我们去做更早的设计决策。

What could be done?
Well, a lot, actually! To make a gross oversimplification, networks will rely on
a few mechanisms to keep working, such as:
1.out of band signaling and messaging to communicate about health (i.e. ICMP messages)
2.estimation of quality of service via observation of in-band messages
3.adjusting of scheduling and sending of messages according to 1 and 2

我们能做什么？
好吧，实际上我们能做的有很多。简单来说，为了保持网络的正常工作，依赖于下面几个机制：
1、为了保持通信的健康的带外信号和消息；
2、通过带内消息评估服务质量
3、根据1 2调整策略和消息的发送

A quick note there is that observation of in-band messages, such as going "oh, 
things are starting to take a lot slower than usual, I bet something iffy is 
going on, better slow down a bit", intimately require respect of the end-to-end 
principles. Doing them hop-by-hop tends to be an exercise in frustration, 
specifically because you end up with a lot of challenges similar to those we see 
with processes within a single node. Being able to insert probes at the beginning 
and at the end of the pipeline tends to let you take a snapshot of the system health 
in a simpler way. If you have synchronous communication channels (and therefore 
the sender receives feedback from the entire pipeline as it receives its response 
back -- or does not receive it), then the sender can be your back-pressure mechanism.

有一个带内消息的观察者，应该快速记录这样一条消息：“事情比以前慢了很多，我敢说可能
哪里出了问题，最好慢点发”，这要求我们遵守端到端的原则。逐跳的做这个事情将成为一个
失败的范例。与我们在单个节点观测的类似，会遇到很多挑战。在流水线的首节点和尾节点
能够插入一个探针的这种做法，让你有一种简单的方式获得系统的一个快照，如果你拥有同步
通信通道，发送者就是你的反压机制。

For a lot of server-side software though, the overall consensus is that you cannot 
trust clients, or at least not all of them. What you can do instead is have a kind 
of middle-man component that will take client requests, and then forward them to 
the rest of the cluster in a more trustable manner.

虽然对于大量的服务端软件的总体共识是：你不能信任客户端至少不能信任所有的客户端，
你可以做的就是，创建一组中间人组件，他来接收客户端的请求，然后把他们以一个更加可
信任的方式发送给集群中的其他节点。

Related Reading
•End-to-end principle
•Bandwidth management
•TCP tuning
•Bufferbloat

Applying to Our Systems

应用到我们的系统

It would be a bit difficult to port all of the expertise embedded in the world's 
networks into our application layer stuff and make effective use of it. It's mostly 
impractical and the problem space is huge. What is interesting in borrowing from 
networks (at least one of the many things) is the idea of probing for quality of 
service and adjusting behaviour accordingly.

把所有的实践嵌入到世界的网络中，使之应用到我们的应用层并且得到有效利用，是有点
困难的。问题域如此巨大，因此这种想法有些不切实际。探测网络服务质量并且做出相应
调整的想法，是我们从网络借鉴中所关注的。

The tricky aspect of probing for performance is choosing how to do it; given all 
tasks in a system are not equal (some take longer, some take shorter) except in 
rare circumstances, probing at the beginning and then the end of a request 
(the total duration) is not a practical metric.

探测性能极其困难的地方在于如何去做他们：除了一些极其罕见的场景外，给定的系统中的
所有任务执行时间各不相同，有的长，有的短，在请求的开始和结束分别进行探测，
并不是一个行之有效的度量。

A more general mechanism for that will often require to rely on a queue being used, 
with a probe put at either end of the queue:

对于此，一个更为普遍的机制通常是需要一个队列，在队列的两端各放一个探针。

What the probe will do is calculate the sojourn time of a given task within the 
queue. Unlike the duration of a single request, tracking the time in the queue 
gives an instant overall wait time within the system based on its capacity to 
extract any number of tasks from the queue.

这个探针所要做的事情就是计算一个给定任务在队列中的执行时间。不像单请求所执行的时间，
在队列里跟踪时间给出一个在系统内的一个恒定的总体等待时间，这个时间是基于从队列中
提取任何数量的任务。

The system can then be configured with a way to adaptively handle a queue filling 
up with increasing sojourn time: the longer the sojourn, the more overloaded the 
system and the more constrained it should be.

系统可以被配置为随着系统逗留时间的增加，自适应的添加这个队列：逗留时间越长，系统
负载越重，越应该受到限制。

•if we aim for a low overall latency for newer request but don't care to have 
long waits for some (i.e. phone call systems for support often deal with this) 
turning the queue into a stack automatically helps

如果我们的目标是对新的请求有个低的总体延迟，但是对某些请求不在乎长时间的等待，
我们只需要把队列修改成堆栈，自然就帮我们解决了这个问题。

•provide an upper timeout; all requests that have waited more than a given period 
of time are considered stale and dropped (load-shedding)

提供一种超时上限，超过这个给定时间的请求被认为是过期并且丢掉。

•prevent enqueuing requests when the sojourn time is too high (back-pressure)

当逗留时间太高的时候禁止入队请求

•randomly drop requests within the queue with increasing aggressiveness as sojourn 
time increases (load-shedding)

随着逗留时间的增加，随机丢掉一些排在队列中的请求

•randomly drop requests before enqueuing them with increasing aggressiveness as 
sojourn time increases (back-pressure)

随着逗留时间的增加，随机丢掉一些在出队前的一些请求

•implement the CoDeL algorithm (works well with multiple queues in a system)

实现CoDel算法

Things to note here include that the idea of load-shedding tends to require that 
either the workload being sent is possible to ignore and lose, or otherwise 
requires client-side cooperation to eventually retry failing operations; the idea 
there being that we assume that eventually, the system will have enough capacity 
to do its job. There are cases where this will not be true, although they may be 
rarer overall.

这里我们需要注意的事情是：负载均衡的想法，或者发送的负载可以被忽略或者丢失；或者
是客户端存在这样的协作，即始终能够对失败的操作进行重发；这个想法假设我们的系统
始终具备足够的能力来处理这些工作。然而，尽管总体上很少，但是仍有这种假设不成立的情况。

The other thing worth mentioning is that randomly dropping requests is a much more 
complex approach than what could initially be believed. Even though it's possible 
to implement very naive and functional versions, algorithms like RED, MRED, ARED, 
RRED, Blue, PIE, and so on, all more or less include small but important variations 
in how the random rate is calculated and modified to account for metrics.

另外一个值得注意的就是，随机丢弃请求的方案比我们起初设想的更加复杂一些。尽管实现
一个比较简单基础的版本是可能的，算法例如：RED, MRED, ARED, RRED, Blue, PIE等等，
这些方法。从度量考虑，如何进行计算和修正随机速率， 多多少少都包含一些小的，但是
重要的变化。

CoDeL is a bit of a special case in that it essentially requires no configuration. 
It can be used both as a back-pressure and load-shedding mechanism depending on 
whether you wait on theacceptance of the task and the specific implementation 
used is able to notify you when a specific task has been dropped off the queue.

CoDel是为了一些少数特殊的场景，这些场景的本质上的需求是无需配置。这种方式既可以
应用在反压机制，也可以应用在负载均衡机制上面，这取决于你是否等待任务的接受，并且
你采用的特定的实现是否能够在特定的任务从队列中丢弃的时候，能够通知你。

These systems tend to be fairly interesting for a lot of tasks, but as for before, 
having the ability to insert random asynchronous points in a pipeline can accidentally 
undo work.

对于多任务来说这些系统变得非常有趣，但是对于在流水线上能够插入随机异步点而言，
会突然变得无法工作。

How to do this in Erlang
The following libraries implement various of the above mechanisms:
sbroker
safetyvalve
jobs


Capacity Probes

探针的容限

When you get to all these issues, it starts being interesting to simply ask the 
question: can I force a reaction or shut down in my system when some key metrics
are too high? For example, can work be dropped when memory gets high, when CPU 
usage is too important, when we detect that send buffers for the network get 
filled, or that database pool capacities are exhausted?

当你获得了所有观点，不禁要问这样一个问题：当某些关键的衡量指标太高的时候，我在我
的系统中强制作出一种反应或者直接关闭。当内存已经很高，cpu利用率过高，发送buffer
已经被网络填满，或者数据库容限已经被耗尽，我们的工作能被丢弃吗？

Of course this is doable. Some of the metrics are more general and shared across 
applications (CPU, memory, scheduler usage, and so on) and some are very specific 
to current apps (pool usage, specific API failure rates, etc.)

当然这是可行的，一些指标是所有应用共享的，例如cpu内存等等，而另外一些是当前app特有的

This gets to play in the specialty of multiple components, specifically circuit 
breakers and work-regulation frameworks that allow sampling.

这能达到在专业的多组件中扮演重要角色，尤其是允许采样的断路器模式和轮训周期工作框架

The former case is simpler to understand. A circuit breaker is basically a gatekeeper 
to some operation that will track a given metric: failure rate, timeout delays, 
and similar metrics. When a failure rate judged to be too high is detected, the 
circuit breaker instantly prevents specific operations from taking place and 
returns an error instead.

前面的例子很容易理解。断路器是一些操作的看守者，这些操作是否被执行依赖于一些给定
的指标：失败率、超时等一些类似的度量。当失败率过高被检测到，断路器瞬间阻止特定的
操作发生而不是返回错误。

They can be thought of as components that work as efficiently as counters, but 
work on a different metric than a maximal value.

他们可以被认为是随着计数器而有效工作的组件，但是工作在一个不同的度量体系而不是最大值

A naive circuit-breaker usage may be, for example, to say that if a given call to 
a web service fails more than 3 times in 10 seconds, we assume it's down and prevent 
all attempts for 5 seconds. We could also say that if it takes more than 750ms to 
get a result back, we consider the remote service to be busy and consider such a 
call to be a failure.

例如一个简单的断路器的例子，如果一个给定的网络服务在10s内失败超过3次，我们假定这
个服务已经down，在5s内阻止所有尝试。当我们花费超过750ms获得一个结果才返回，我们
认为远端服务忙，并且任务这样一次调用为一次失败。

Circuit-breakers are a very important component of building stable systems, but 
what is interesting is that they often allow a mode by which you can manually 
trip the breaker; for as long as you desire, the incoming calls are seen as forced
to fail. The reason this is interesting is that we can use this property to build 
probes and agents that will control certain breakers. Then, by looking at all the 
breakers when entering the system, we can know if it is worth scheduling a given 
ask or denying it:

断路器在建立稳健系统里面非常重要，但是非常有趣的一点是，他们经常允许一种模式，就是
你可以手动操作断路器，只要你想要，进来的调用都被看做失败。一个非常有趣的原因就是
我们可以利用这个属性建立探测器和一些代理和控制某些探针。当断路器进入系统，我们可以
通过查看断路器，来知道一个给定的任务，我们需要调度它，还是禁止它。

By putting the probes in place in all critical areas of the system or on key metrics, 
we can then cut and trip circuit breakers as we see fit. This lets us create 
surprisingly simple and efficient mechanisms by which we can be allowed to have 
parts of the system become asynchronous and bypass common mechanisms, as long as
the consequences of overload deep down are somehow captured by one of the circuit 
breakers.

通过在系统所有关键的地方或者关键的度量安放探针，我们可以根据我们自己觉得是否合适
，来打开或者关闭断路器。这让我们创建了令人吃惊的简单且有效的机制，通过这个机制，
我们可以允许我们的系统有某些部分是异步的，并且旁路一些通用的机制，只要某些方面
被一个断路器捕获，就能达到负载下降的结果。

In the case of ever-increasing mailboxes for example, memory could easily play that role.

以前面提到的消息邮箱的例子，内存可能扮演者这个角色

One caveat of using circuit breakers though is that they tend to be an all-or-nothing
mechanism. Their use can therefore create flappy systems that will react somewhat 
violently to probes melting fuses. A simple way to try and work around it is to use 
queues as a dampener: put the probe checks before the queue as a kind of barrier 
(the last of which could be a queue size, to include the counter approach!), 
and as the queue gets drained through blown fuses, their individual effect becomes 
more like a regulator than a full stop. They will be a perfect match when the 
cause for a failure is pretty major, such as a remote service being entirely down.

使用断路器的一个警告，断路器是一种：或者所有或者全无的机制，应用断路器可以创建一
个飞扬的系统，这个系统可以在熔断的时候响应的有点剧烈。一种简单的试着绕过它的方式是
使用队列作为一种阻尼器：把探针放在队列的前面，作为队列的一种障碍，当队列耗尽的时候
，拉断保险丝，他们各自的功效变得更像一个调节器而不是全停止。当一个相当大的失效所
引起的失败的时候，例如远端服务全部down掉，这种方式将会非常匹配。

A more systematic approach to resolve that is the one seen in the jobs framework. 
Rather than manually installing a bunch of circuit breakers, the queue for the 
overall system can see its limits and inputs augmented by samplings of various 
metrics (once again, such as CPU or memory).

解决这种问题的一种更系统化的方式是：在jobs框架中所看到的。与其手动安装大量的断路器，
还不如通过采集各种度量指标，使得为整个系统建立的队列，能够看到这些限制和输入参数。

What the framework will do then is use the samples taken from various probes around 
the system, and usethem as a weighing mechanism to choose a rate at which the job 
queue may be consumed. As consuming of the queue is modulated by these metrics, 
it may be allowed to fill up to a point where requests are directly denied.

框架所要做的将是，围绕着系统的各式各样的探针所采集的数据，利用这些数据作为一种
衡量机制，选择出哪个job的队列可能消耗的速率。队列的估算被这些度量模型化，在请求
被直接拒绝的地方，它或许被运行增加一个点。

To add to this, such frameworks will typically be employed at the edge of the 
system, not throughout.By being able to take into account various internal metrics 
and characteristics of the system, we can get a general configurable policy in 
one spot, which then protects the rest of the system, allowing for a clearer 
implementation for the rest of it (where you may actually encourage asynchronous 
behaviours!)

至于增加探针，这个框架将会部署在系统的边界而不是自始至终。为了能够考虑系统各种各样
的内部衡量指标和特性，我们可以在一个地点获得一个大概的配置策略，然后保护系统的
其余部分，允许系统的其余部分有一个更清晰的实现。

What's interesting is that both approaches are not mutually exclusive. Circuit 
breakers are usually extremely cheap to run and check, and so combining them with 
another approach is a great way to preserve overall system balance when major 
failures happen in upstream components and services, a case where the guaranteed 
failure of the operation would likely be much more expensive to account for within 
the queue management frameworks themselves.

我们感兴趣的是这两种方法不是互相冲突的。断路器通常运行和检测及其廉价，因此,当上游
组件或者服务端发生主要故障时候，采用两种方式结合起来，来保持整个系统的平衡，是一种
很好的方式.有一种场景，就是在确保失败的时候，考虑到在队列框架内部自己管理是十分昂贵
的情况。

How to do this in Erlang
The most commonly seen circuit breaker libraries are:
Klarna's circuit_breaker
fuse
breaky
And the one tool to do full blown job management there is Ulf Wiger's jobs framework.

Conclusion

结论

There's a lot of possible solutions for all kinds of tasks and workloads here. What I 
hoped to show with this text is that there is a rich zoo of overload problems and 
potential solutions that can be applied to solve them, ranging far beyond a simple queue 
boundary for mailboxes. By reasoning and looking at your system, it becomes interesting 
to figure out which approach is best: a small system with some relatively infrequent 
load-spikes may be fine with a queue or a pull-based mechanism. An all-synchronous system 
may be fine with simple counter-based protection for most tricky cases.

对于各种各样的任务和负载，这里提供了各种可能的解决方案。这篇文章我所希望展示的是
有各种各种的负载问题，以及我们可以解决这些问题的潜在的解决方式，远非简单的队列边界。
通过分析和查看你的系统，估算哪种方法是最好的，对于一个小很少遇见负载尖峰的系统或许
使用队列或者基于pull的机制就比较好了，对于一个全同步的系统，对于最复杂的场景，应用
基于计数的保护方式就比较好了

Complex systems may, on the other hand, require a more involved approach, especially when 
analyzing the overall workflow is complex. The solutions can range from active queue 
management to gathering all kinds of metrics or probes and enriching the scheduling or 
regulating mechanisms of the system with them. They can also be combined together to produce 
very performant solutions without requiring titanesque effort.

另一方面，一个复杂的系统或许需要一个更复杂的方法，尤其是分析整个工作流十分复杂的情况。
这种解决方案可能涉及：采集各种度量信息的激活队列，或者探针 和 丰富的调度 或者各种
系统中应用这些的各种调节策略。他们也可以合并起来，不需要很大的努力而获得非常巨大
功效。
